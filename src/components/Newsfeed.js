import React, { Component } from 'react';
import { View } from 'react-native';
import NewsfeedDetail from './NewsfeedDetail';

class Newsfeed extends Component {
render() {
  return (
    <View style={{ backgroundColor: '#fff' }} >
      <NewsfeedDetail />
    </View>
  );
  }
}

export default Newsfeed;
