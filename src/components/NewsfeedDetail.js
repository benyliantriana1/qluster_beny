import React from 'react';
import { Text, View, Image } from 'react-native';
import { CardSection, Card } from './common';

const image = require('../assets/fb.png');
const marker = require('../assets/marker_small.png');
const likes = require('../assets/likes.png');
const share = require('../assets/share.png');

const NewsfeedDetail = () => {
  //const { title, artist, thumbnail_image, image, url } = album;
  const { headerContentStyle,
          thumbnailStyle,
          thumbnailContainerStyle,
          headerTextStyle,
          textStyle,
          commentTextStyle,
          descriptionTextStyle,
          containerMap,
          imageStyle } = styles;
  return (
    <Card>
      <CardSection>
        <View style={thumbnailContainerStyle}>
          <Image
            style={thumbnailStyle}
            source={image}
          />
        </View>
        <View style={headerContentStyle}>
          <Text style={headerTextStyle}>2 jam yang lalu</Text>
          <Text style={textStyle}>Marisa Sidiq</Text>
          <Text style={commentTextStyle}>10 likes</Text>
        </View>
      </CardSection>

      <CardSection>
        <Image
          style={imageStyle}
          source={image}
        />
      </CardSection>

      <CardSection>
        <View style={headerContentStyle}>
          <Text style={descriptionTextStyle}>Deskripsi</Text>
        </View>
      </CardSection>

      <CardSection>
        <View style={containerMap} >
          <Image
            style={{ width: 24, height: 34, marginRight: 2 }}
            source={marker}
          />
          <View style={headerContentStyle}>
            <Text style={descriptionTextStyle}>Alamat</Text>
          </View>
          <Image
            style={{ width: 31, height: 31, marginRight: 2 }}
            source={share}
          />
          <Image
            style={{ width: 31, height: 31, marginRight: 2 }}
            source={likes}
          />
          <Text style={{ marginTop: 6 }}>10</Text>
        </View>
      </CardSection>
    </Card>
  );
};

const styles = {
  headerContentStyle: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    flex: 1
  },
  headerTextStyle: {
    fontSize: 12,
    color: '#bcc6c0'
  },
  textStyle: {
    fontSize: 16,
    color: '#000'
  },
  descriptionTextStyle: {
    fontSize: 16,
    color: '#000'
  },
  thumbnailStyle: {
    height: 50,
    width: 50,
    marginRight: 10
  },
  commentTextStyle: {
    fontSize: 10,
    color: '#bcc6c0',
    alignSelf: 'flex-end'
  },
  containerMap: {
    borderTopWidth: 1,
    borderTopColor: '#e0e2e1',
    flex: 1,
    flexDirection: 'row',
    marginTop: 5,
    paddingTop: 5
  },
  thumbnailContainerStyle: {
    justifyContent: 'flex-start',
  },
  imageStyle: {
    height: 220,
    flex: 1,
    width: 220
  }
};

export default NewsfeedDetail;
