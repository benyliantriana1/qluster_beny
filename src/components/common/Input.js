import React from 'react';
import { TextInput, View, Text } from 'react-native';

const Input = ({ label, value, onChangeText, placeholder, secureTextEntry }) => {
  const { inputStyle, labelStyle, containerStyle } = styles;
  return (
    <View
      style={containerStyle}
    >
      <Text style={labelStyle}>{label}</Text>
      <TextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        underlineColorAndroid='transparent'
        autoCorrect={false}
        value={value}
        onChangeText={onChangeText}
        style={inputStyle}
      />
    </View>
  );
};

const styles = {
  inputStyle: {
    color: '#000',
    fontSize: 18,
    flex: 2,
    lineHeight: 23
  },
  labelStyle: {
    marginBottom: 5,
    color: '#000',
    marginTop: -1,
    fontSize: 18,
    flex: 1,
    paddingLeft: 5,
    marginRight: 10
  },
  containerStyle: {
    height: 40,
    flexDirection: 'row',
    alignItems: 'center'
  }
};

export { Input };
