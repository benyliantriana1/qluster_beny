import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import FacebookTabBar from './FacebookTabBar';
import Newsfeed from './components/Newsfeed';

class App extends Component {
      render() {
        return (
        <View style={styles.view}>
          <View style={styles.container}>
          </View>
          <ScrollableTabView
          initialPage={1}
          renderTabBar={() => <FacebookTabBar />}
          >
          <ScrollView tabLabel="ios-home" style={styles.tabView}>
            <View style={styles.card}>
              <Text>News new</Text>
            </View>
          </ScrollView>
          <ScrollView tabLabel="ios-people" style={styles.tabView}>
            <View style={styles.card}>
              <Newsfeed />
            </View>
          </ScrollView>
          <ScrollView tabLabel="ios-chatboxes" style={styles.tabView}>
            <View style={styles.card}>
              <Text>Messenger</Text>
            </View>
          </ScrollView>
          <ScrollView tabLabel="ios-notifications" style={styles.tabView}>
            <View style={styles.card}>
              <Text>Notifications</Text>
            </View>
          </ScrollView>
        </ScrollableTabView>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  tabView: {
    flex: 1,
    padding: 10,
    backgroundColor: 'rgba(0,0,0,0.01)',
  },
  card: {
    backgroundColor: '#fff',
  },
  view: {
    flex: 1
  },
  container: {
    height: 100,
    backgroundColor: '#239b49'
  }
});

export default App;
